package com.viking.soscondo

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.viking.soscondo.Models.User
import com.viking.soscondo.ui.login.LoginActivity

import kotlinx.android.synthetic.main.content_main.*

class RegisterActivity : AppCompatActivity() {

    lateinit var database: FirebaseDatabase
    lateinit var firebaseAuth: FirebaseAuth
    private var mDatabaseReference: DatabaseReference? = null
    private var mDatabase: FirebaseDatabase? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
//        setSupportActionBar(toolbar)

//        toolbar.setTitle(R.string.app_name)

        firebaseAuth = FirebaseAuth.getInstance()
        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference!!.child("Users")


        signUp.setOnClickListener {
            signup()
        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this@RegisterActivity, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
    }

    private fun signup() {
        Log.d("test", "fn:" + firstNameText.text)
        print(firstNameText.text)
        if (doFormValidation()) {
            Log.d("test", "form ok!")
            firebaseAuth.createUserWithEmailAndPassword(emailText.text.toString(), password1Text.text.toString())
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        Log.d("test", "createUserWithEmail:success")
                        val userId = firebaseAuth!!.currentUser!!.uid
                        val currentUserDb = mDatabaseReference!!.child(userId)
                        val user = User(
                            firstNameText.text.toString(),
                            lastNameText.text.toString(),
                            telText.text.toString(),
                            emailText.text.toString(),
                            unitText.text.toString(),
                            floorText.text.toString(),
                            associationText.text.toString()
                        )
                        currentUserDb.setValue(user).addOnCompleteListener { task ->
                            if (task.isSuccessful) {
//                                finish()
                                val intent = Intent(this@RegisterActivity, LoginActivity::class.java)
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                startActivity(intent)
                                Toast.makeText(this, getString(R.string.registration_success), Toast.LENGTH_LONG).show()

                            } else {
                                Log.e("test", "registration failed" + task.result + " " + task.exception)
                            }
                        }
//                        currentUserDb.child("firstName").setValue(firstNameText.toString())
//                        currentUserDb.child("lastName").setValue(lastNameText.toString())
                    } else {
                        Toast.makeText(this, task.exception!!.localizedMessage, Toast.LENGTH_LONG).show()

                        Log.e("test", "create user failed: " + task.result + " " + task.exception)
                    }
                }
        } else {
            Log.d("test", "form not ok!")
        }


    }

    private fun doFormValidation(): Boolean {
        var message: String = ""

        if (firstNameText.text.isBlank()) {
            message = getString(R.string.first_name_missing)
        } else if (lastNameText.text.isBlank()) {
            message = getString(R.string.last_name_missing)
        } else if (emailText.text.isBlank()) {
            message = getString(R.string.email_missing)
        } else if (telText.text.isBlank()) {
            message = getString(R.string.tel_missing)
        } else if (unitText.text.isBlank()) {
            message = getString(R.string.unit_no_missing)
        } else if (floorText.text.isBlank()) {
            message = getString(R.string.floor_no_missing)
        } else if (associationText.text.isBlank()) {
            message = getString(R.string.association_missing)
        } else if (password1Text.text.isBlank()) {
            message = getString(R.string.password_missing)
        } else if (password2Text.text.isBlank()) {
            message = getString(R.string.password_repeat_missing)
        } else if (telText.length() != 10) {
            message = getString(R.string.tel_invalid)
        } else if (password1Text.length() < 6) {
            message = getString(R.string.password_invalid)
        } else if (!password1Text.text.toString().equals(password2Text.text.toString())) {
            var p1 = password1Text.text.toString()
            var p2 = password2Text.text.toString()
            Log.d("test", "p1:"+p1+" p2: "+ p2)
            message = getString(R.string.password_repeat_error)
        }

        if (message != "") {
            Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
            return false
        }

        return true
    }

}


