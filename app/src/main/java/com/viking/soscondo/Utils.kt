package com.viking.soscondo

import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.viking.soscondo.Models.User
import kotlinx.android.synthetic.main.activity_main.*

class Utils {

    var mAuth: FirebaseAuth? = null
    lateinit var usersReference: DatabaseReference
    lateinit var database: FirebaseDatabase



    private fun loadUserInformation() {
        val user: FirebaseUser? = mAuth?.currentUser
        var userReturn: User?


//        user?.let {
//            for (profile in it.providerData) {
//                profile.uid
//            }
//        }

        if (user != null) {
            val reference =  usersReference.child(user.uid)

            reference.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val first = dataSnapshot.child("firstName")
                    val userData: User? = dataSnapshot.getValue<User>(User::class.java)

                    userReturn = userData

                }

                override fun onCancelled(databaseError: DatabaseError) {
                    // ...
                }
            })
        }


    }
}