package com.viking.soscondo.ui.login

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*
import android.content.Intent
import com.viking.soscondo.RegisterActivity
import android.util.Log
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.viking.soscondo.HomeActivity
import com.viking.soscondo.Models.User


class LoginActivity : AppCompatActivity() {

    private lateinit var loginViewModel: LoginViewModel
    var mAuth: FirebaseAuth? = null
    lateinit var usersReference: DatabaseReference
    lateinit var database: FirebaseDatabase



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(com.viking.soscondo.R.layout.activity_login)
        mAuth = FirebaseAuth.getInstance()

        database = FirebaseDatabase.getInstance()
//        usersReference = database.getReference("Users")
        usersReference = database!!.reference!!.child("Users")

        login.isEnabled = true
        loginViewModel = ViewModelProviders.of(this, LoginViewModelFactory())
            .get(LoginViewModel::class.java)

        registerButton.setOnClickListener {
//            finish()
            val intent = Intent(this@LoginActivity, RegisterActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }


//        username.setText("test1@gmail.com")
//        password.setText("password1")

//        loadUserInformation()


            username.afterTextChanged {
            loginViewModel.loginDataChanged(
                username.text.toString(),
                password.text.toString()
            )
        }

        loginViewModel.loginFormState.observe(this@LoginActivity, Observer {
            val loginState = it ?: return@Observer

            // disable login button unless both username / password is valid
//            login.isEnabled = loginState.isDataValid

            if (loginState.usernameError != null) {
                username.error = getString(loginState.usernameError)
            }
            if (loginState.passwordError != null) {
                password.error = getString(loginState.passwordError)
            }
        })


        login.setOnClickListener {
            doOnLogin()
        }

        /*val username = findViewById<EditText>(com.viking.soscondo.R.id.username)
        val password = findViewById<EditText>(com.viking.soscondo.R.id.password)
        val login = findViewById<Button>(com.viking.soscondo.R.id.login)
        val loading = findViewById<ProgressBar>(
            com.viking.soscondo.R.id.loading)



        loginViewModel.loginFormState.observe(this@LoginActivity, Observer {
            val loginState = it ?: return@Observer

            // disable login button unless both username / password is valid
            login.isEnabled = loginState.isDataValid

            if (loginState.usernameError != null) {
                username.error = getString(loginState.usernameError)
            }
            if (loginState.passwordError != null) {
                password.error = getString(loginState.passwordError)
            }
        })

        loginViewModel.loginResult.observe(this@LoginActivity, Observer {
            val loginResult = it ?: return@Observer

            loading.visibility = View.GONE
            if (loginResult.error != null) {
                showLoginFailed(loginResult.error)
            }
            if (loginResult.success != null) {
                updateUiWithUser(loginResult.success)
            }
            setResult(Activity.RESULT_OK)

            //Complete and destroy login activity once successful
            finish()
        })



        password.apply {
            afterTextChanged {
                loginViewModel.loginDataChanged(
                    username.text.toString(),
                    password.text.toString()
                )
            }

            setOnEditorActionListener { _, actionId, _ ->
                when (actionId) {
                    EditorInfo.IME_ACTION_DONE ->
                        loginViewModel.login(
                            username.text.toString(),
                            password.text.toString()
                        )
                }
                false
            }

            login.setOnClickListener {
                loading.visibility = View.VISIBLE
                loginViewModel.login(username.text.toString(), password.text.toString())
            }
        }
        */
    }

    private fun loadUserInformation() {
        val user: FirebaseUser? = mAuth?.currentUser
        var userReturn: User?


//        user?.let {
//            for (profile in it.providerData) {
//                profile.uid
//            }
//        }

        if (user != null) {
            val reference =  usersReference.child(user.uid)

            reference.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val userData: User? = dataSnapshot.getValue<User>(User::class.java)

                    Log.d("test", "already logged in as " + userData?.firstName)
//                    finish()
                    val intent = Intent(this@LoginActivity, HomeActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)

                }

                override fun onCancelled(databaseError: DatabaseError) {
                    // ...
                }
            })
        }


    }

    private fun doOnLogin() {
//        val email = editTextEmail.getText().toString().trim()
//        val password = editTextPassword.getText().toString().trim()

        if (username.text.isEmpty()) {
            username.error = "Error!"
            username.requestFocus()
            return
        }
//
//        if (!Patterns.EMAIL_ADDRESS.matcher(username.text).matches()) {
//            editTextEmail.setError("Please enter a valid email")
//            editTextEmail.requestFocus()
//            return
//        }

//        if (password.isEmpty()) {
//            username.setError("Password is required")
//            username.requestFocus()
//            return
//        }

        if (password.text.length < 6) {
            password.error = "Minimum lenght of password should be 6"
            password.requestFocus()
            return
        }

        loading.visibility = View.VISIBLE

        mAuth?.signInWithEmailAndPassword(username.text.toString(), password.text.toString())
            ?.addOnCompleteListener { task ->
                loading.visibility  = View.GONE
                if (task.isSuccessful) {
//                    finish()
                    val intent = Intent(this@LoginActivity, HomeActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                } else {
                    Toast.makeText(applicationContext, task.exception!!.localizedMessage, Toast.LENGTH_SHORT).show()
                }
            }
    }

    private fun updateUiWithUser(model: LoggedInUserView) {
        val welcome = getString(com.viking.soscondo.R.string.welcome)
        val displayName = model.displayName
        // TODO : initiate successful logged in experience
        Toast.makeText(
            applicationContext,
            "$welcome $displayName",
            Toast.LENGTH_LONG
        ).show()
    }

    private fun showLoginFailed(@StringRes errorString: Int) {
        Toast.makeText(applicationContext, errorString, Toast.LENGTH_SHORT).show()
    }
}

/**
 * Extension function to simplify setting an afterTextChanged action to EditText components.
 */
fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}
