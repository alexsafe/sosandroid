package com.viking.soscondo

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.viking.soscondo.Models.User
import com.viking.soscondo.ui.login.LoginActivity
import kotlinx.android.synthetic.main.activity_loading.*


class LoadingActivity : AppCompatActivity() {

    var mAuth: FirebaseAuth? = null
    lateinit var usersReference: DatabaseReference
    lateinit var database: FirebaseDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loading)

        mAuth = FirebaseAuth.getInstance()
        database = FirebaseDatabase.getInstance()
        usersReference = database!!.reference!!.child("Users")


        progressbar.visibility = View.VISIBLE

        loadUserInformation()


    }

    private fun loadUserInformation() {
        val user: FirebaseUser? = mAuth?.currentUser
        var userReturn: User?


//        user?.let {
//            for (profile in it.providerData) {
//                profile.uid
//            }
//        }

        if (user != null) {
            val reference =  usersReference.child(user.uid)

            reference.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val userData: User? = dataSnapshot.getValue<User>(User::class.java)

                    Log.d("test", "already logged in as " + userData?.firstName)
                    if (userData?.firstName != null) {
                        progressbar.visibility = View.GONE
//                    finish()
                        val intent = Intent(this@LoadingActivity, HomeActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)
                    } else {
                        progressbar.visibility = View.GONE

//                    finish()
                        val intent = Intent(this@LoadingActivity, LoginActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)
                    }

                }

                override fun onCancelled(databaseError: DatabaseError) {
                    progressbar.visibility = View.GONE

//                    finish()
                    val intent = Intent(this@LoadingActivity, LoginActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                }
            })
        } else {
            progressbar.visibility = View.GONE
//                    finish()
            val intent = Intent(this@LoadingActivity, LoginActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }


    }
}
