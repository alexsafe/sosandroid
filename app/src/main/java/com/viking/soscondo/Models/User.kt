package com.viking.soscondo.Models

import com.google.firebase.database.ServerValue


class User {

    var firstName: String = ""
    var lastName: String = ""
    var tel: String = ""
    var email: String = ""
    var apNo: String = ""
    var floorNo: String = ""
    var association: String = ""

    // Default constructor required for calls to DataSnapshot.getValue(User.class)
    constructor()

    constructor(
        firstName: String,
        lastName: String,
        tel: String,
        email: String,
        apNo: String,
        floorNo: String,
        association: String
    ) {
        this.firstName = firstName
        this.lastName = lastName
        this.tel = tel
        this.email = email
        this.apNo = apNo
        this.floorNo = floorNo
        this.association = association
    }

//    fun createMessageObj(): Map<String, Any> {
//        val messageObj = HashMap<String, Any>()
//        messageObj["date"] = ServerValue.TIMESTAMP
//        messageObj["messageText"] = this.messageText
//        messageObj["user"] = this.user
//        return messageObj
//    }
}