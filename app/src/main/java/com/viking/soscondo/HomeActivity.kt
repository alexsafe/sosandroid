package com.viking.soscondo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.content.Intent
import android.net.Uri
import android.support.v4.content.ContextCompat
import android.Manifest
import android.content.pm.PackageManager
import android.graphics.PorterDuff
import android.support.v4.app.ActivityCompat
import android.view.MotionEvent
import com.viking.soscondo.Models.User
import com.google.firebase.database.*
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_register.*
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.DatabaseReference
import com.viking.soscondo.ui.login.LoginActivity

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.support.v4.app.SupportActivity
import android.support.v4.app.SupportActivity.ExtraData
import android.support.v4.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout


class HomeActivity : AppCompatActivity() {


    private val TAG = "PhoneHome"
    private val RECORD_REQUEST_CODE = 101
    //    private lateinit var database: DatabaseReference
    lateinit var database: FirebaseDatabase
    lateinit var usersReference: DatabaseReference
    var mAuth: FirebaseAuth? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var firebaseDatabase: FirebaseDatabase = FirebaseDatabase.getInstance()
        mAuth = FirebaseAuth.getInstance()

        database = FirebaseDatabase.getInstance()
//        usersReference = database.getReference("Users")
        usersReference = database!!.reference!!.child("Users")

        loadUserInformation()


        setupPermissions()

        val btn1 = findViewById<Button>(R.id.button1)
        val btn2 = findViewById<Button>(R.id.button2)
        val btn3 = findViewById<Button>(R.id.button3)
//        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED
//        ) {


//            btn1.setOnTouchListener { v, motion -> test() }


//            btn1.setOnTouchListener { v, event -> doOnclick(btn1, v, event, Constants.tel1) }


//            btn1.setOnTouchListener { v, event ->
//                when (event.action) {
//                    MotionEvent.ACTION_DOWN -> {
//                        val scaleDownX = ObjectAnimator.ofFloat(
//                            btn1,
//                            "scaleX", 0.7f
//                        )
//                        val scaleDownY = ObjectAnimator.ofFloat(
//                            btn1,
//                            "scaleY", 0.7f
//                        )
//                        scaleDownX.duration = 500
//                        scaleDownY.duration = 500
//
//                        val scaleDown = AnimatorSet()
//                        scaleDown.play(scaleDownY).with(scaleDownY)
//
//                        scaleDown.start()
//
//
////                        val params: ViewGroup.LayoutParams = btn1.layoutParams
////                        params.height = 40
////                        btn1.layoutParams = params
//
//
//
//                        v.invalidate()
//                    }
//                    MotionEvent.ACTION_UP -> {
//
//                        val scaleDownX2 = ObjectAnimator.ofFloat(
//                            btn1,
//                            "scaleX", 1.0f
//                        )
//                        val scaleDownY2 = ObjectAnimator.ofFloat(
//                            btn1,
//                            "scaleY", 1.0f
//                        )
//                        scaleDownX2.duration = 500
//                        scaleDownY2.duration = 500
//
//                        val scaleDown2 = AnimatorSet()
//                        scaleDown2.play(scaleDownY2).with(scaleDownY2)
//
//                        scaleDown2.start()
//
//                        v.invalidate()
//                    }
//                }
//                false
//            }

        button1.setOnClickListener { v ->
            doPhoneCall(Constants.tel1)
        }

        button2.setOnClickListener { v ->
            doPhoneCall(Constants.tel1)
        }

        button3.setOnClickListener { v ->
            doPhoneCall(Constants.tel1)
        }
//        }

        if (FirebaseAuth.getInstance().currentUser != null) {
            Log.d("test", "current user: " + FirebaseAuth.getInstance().currentUser)
            welcome.text = FirebaseAuth.getInstance().currentUser?.displayName
        }

//        testFirebase()
    }


    private fun doClickAnimation() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this@HomeActivity, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
    }

    private fun doOnclick(btn1: Button, v: View, motion: MotionEvent, telNO: String): Boolean {
        doButtonClickAnimation(btn1, v, motion)
//        doPhoneCall(telNO)
        return true
    }

    private fun doButtonClickAnimation(btn1: Button, v: View, event: MotionEvent) {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                val scaleDownX = ObjectAnimator.ofFloat(
                    btn1,
                    "scaleX", 0.7f
                )
                val scaleDownY = ObjectAnimator.ofFloat(
                    btn1,
                    "scaleY", 0.7f
                )
                scaleDownX.duration = 500
                scaleDownY.duration = 500

                val scaleDown = AnimatorSet()
                scaleDown.play(scaleDownY)//.with(scaleDownY)

                scaleDown.start()


//                        val params: ViewGroup.LayoutParams = btn1.layoutParams
//                        params.height = 40
//                        btn1.layoutParams = params


                v.invalidate()
            }
            MotionEvent.ACTION_UP -> {

                val scaleDownX2 = ObjectAnimator.ofFloat(
                    btn1,
                    "scaleX", 1.0f
                )
                val scaleDownY2 = ObjectAnimator.ofFloat(
                    btn1,
                    "scaleY", 1.0f
                )
                scaleDownX2.duration = 500
                scaleDownY2.duration = 500

                val scaleDown2 = AnimatorSet()
                scaleDown2.play(scaleDownY2)//.with(scaleDownY2)

                scaleDown2.start()

                v.invalidate()
            }

        }
        false
    }


    private fun doPhoneCall(telNo: String) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED
        ) {
            val intent = Intent(Intent.ACTION_CALL)

            intent.data = Uri.parse("tel:$telNo")
            applicationContext.startActivity(intent)
        } else {
            makeRequest()
        }
    }

    private fun loadUserInformation() {
        var user: FirebaseUser? = mAuth?.currentUser
//        user?.let {
//            for (profile in it.providerData) {
//                profile.uid
//            }
//        }
//        user.uid
        if (user != null) {
            Log.d("test", "userid:" + user.uid)
            if (user.displayName != null) {
                welcome.text = user.displayName
            }
            val phoneQuery = usersReference.orderByChild(user.uid).equalTo(user.uid)
            val reference =
                usersReference.child(user.uid)//  child("Driver2").child("Device_id_here").child("coordinates").child("date_here")


            reference.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val first = dataSnapshot.child("firstName")
                    val userData: User? = dataSnapshot.getValue<User>(User::class.java)
                    val welcomeText = getString(com.viking.soscondo.R.string.welcome)

                    welcome.text = "$welcomeText, " + userData?.firstName// +" "+userData?.lastName
                    Log.d("test", "first:" + first.toString())


                }

                override fun onCancelled(databaseError: DatabaseError) {
                    // ...
                }
            })
        }


    }

    private fun testFirebase() {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        val database = FirebaseDatabase.getInstance()
        val myRef = database.getReference("message")

        myRef.setValue("Hello, World!")
    }

//
//    public fun saveMessage(text : String) {
//
//        var databaseReference:DatabaseReference = database.getReference()
//        User message = new User("user", text)
//        databaseReference
//            .child("messages")
//            .child(UUID.randomUUID().toString())
//            .setValue(message.createMessageObj());
//    }

    private fun writeNewUser(userId: String, message: String) {
        val message = User("fn", "ln", "12324", "mail@gmail.com", "12", "2", "assoc")
        val userId = usersReference.push().key

        usersReference.child(userId!!).setValue(message)
            .addOnSuccessListener {
                // Write was successful!
                // ...
            }
            .addOnFailureListener {
                // Write failed
                // ...
            }


//        database.child("usersReference").child(userId).setValue(user)
    }

    private fun setupPermissions() {
        val permission = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.CALL_PHONE
        )

        if (permission != PackageManager.PERMISSION_GRANTED) {
            Log.i(TAG, "Permission to record denied")
            makeRequest()
        }
    }

    private fun makeRequest() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.CALL_PHONE),
            RECORD_REQUEST_CODE
        )
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            RECORD_REQUEST_CODE -> {

                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {

                    Log.i(TAG, "Permission has been denied by user")
                } else {
                    Log.i(TAG, "Permission has been granted by user")
                }
            }
        }
    }
}
